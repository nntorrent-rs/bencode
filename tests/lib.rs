extern crate libbencode;
use libbencode::bencode::{decode,BencodeResult};

#[test]
fn is_int_equal_to_1234() {
    let x: Vec<u8> = (vec!['i', '1', '2', '3', '4', 'e']).iter().map(|&y| y as u8).collect();

    let ret: i64 = match decode(x) {
        BencodeResult::BInt(i) => i,
        _       => 0,
    };

    assert_eq!(ret, 1234 as i64);
}
