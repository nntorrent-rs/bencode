extern crate libbencode;
use libbencode::bencode::{BencodeResult, decode};

fn main() {
    let a: Vec<u8> = (vec!['i', '1', '2', '3', '4', 'e']).iter().map(|&y| y as u8).collect();
    let b: Vec<u8> = (vec!['4', ':', 'h', 'e', 'l', 'l', 'o']).iter().map(|&y| y as u8).collect();
    let c: Vec<u8> = (vec!['5', 'h', 'e', 'l', 'l', 'o']).iter().map(|&y| y as u8).collect();

    for i in (vec![x, y, z]).iter() {
        match decode(i) {
            Some(BencodeResult::BInt(a)) => println!("{}", a),
            Some(BencodeResult::BStr(a)) => println!("{}", a),
            Some(_)                      => println!("it's something else"),
            None                         => println!("error decoding"),
        }
    }
}
