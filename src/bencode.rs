use std::collections::HashMap;
use std::vec::Vec;
use std::ascii::Ascii;

pub enum BencodeResult {
    BInt(i64),
    BStr(String),
    BList(Vec<BencodeResult>),
    BDict(HashMap<String, BencodeResult>),
}

pub fn decode(data: &Vec<u8>) -> Option<BencodeResult> {
    if is_int(data)  { return decode_int(data); }
    if is_str(data)  { return decode_str(data); }
    if is_list(data) { return decode_list(data); }
    None
}

/* int */
fn is_int(data: &Vec<u8>) -> bool {
    if data[0] != ('i' as u8) { return false; }
    if data[data.len() - 1] != ('e' as u8) { return false; }

    for i in data.slice(1, data.len() - 1).iter() {
        if !i.to_ascii().is_digit() { return false; }
    }

    true
}

fn decode_int(data: &Vec<u8>) -> Option<BencodeResult> {
    let temp: Vec<Ascii> = data.slice(1, data.len() - 1)
                               .iter()
                               .map(|x| { x.to_ascii() })
                               .collect();

    let res: i64 = match from_str(temp.as_str_ascii()) {
        Some(i) => i,
        None    => return None,
    };

    Some(BencodeResult::BInt(res))
}

/* str */
fn is_str(data: &Vec<u8>) -> bool {
    let x = match find_sep_index(data, ':') {
        Some(i) => i,
        None    => return false,
    };

    for i in range(0u, x) {
        if !data[i].to_ascii().is_digit() { return false; }
    }

    true
}

fn decode_str(data: &Vec<u8>) -> Option<BencodeResult> {
    let x = match find_sep_index(data, ':') {
        Some(i) => i,
        None    => return None,
    };

    /* for some reason, when I try to put the data.slice() fragment
     * inside from_str, I get "the type of this value must be known in this
     * context", so a temp variable is necessary apparently
     */
    let temp: Vec<Ascii> = data.slice(0, x)
                               .iter()
                               .map(|a| { a.to_ascii() })
                               .collect();
    let l: uint = match from_str(temp.as_str_ascii()) {
        Some(i) => i,
        None    => return None,
    };

    let mut ret: String = String::new();
    for i in range(x+1, x+1+l) {
        ret.grow(1, data[i] as char);
    }

    Some(BencodeResult::BStr(ret))
}

fn find_sep_index(data: &Vec<u8>, c: char) -> Option<uint> {
    let mut res = -1;

    for i in range(0u, data.len()) {
        if (data[i] as char) == c { res = i; break; }
    }

    if res == -1 { None }
    else         { Some(res) }
}

/* list */
fn is_list(data: &Vec<u8>) -> bool {
    if (data[0] as char == 'l') && (data[data.len() - 1] as char) == 'e' {
        return true
    }
    false
}

fn decode_list(data: &Vec<u8>) -> Option<BencodeResult> {
    None
}
